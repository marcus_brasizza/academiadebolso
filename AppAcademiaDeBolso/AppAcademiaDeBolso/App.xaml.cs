﻿using AppAcademiaDeBolso.Models;
using AppAcademiaDeBolso.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace AppAcademiaDeBolso
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

            MainPage = new Views.LoginView();
		}

        protected override void OnStart()
        {
            MessagingCenter.Subscribe<UsuarioModel>(this, "SucessoLogin",
                (usuario) =>
                {
                    MainPage = new ViewPaginaInicial(usuario);
                });
        }


        protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
