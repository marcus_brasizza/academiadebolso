﻿using AppAcademiaDeBolso.Models;
using AppAcademiaDeBolso.ViewModel;
using AppAcademiaDeBolso.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppAcademiaDeBolso.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewPaginaInicial : MasterDetailPage
    {


        public List<ViewPaginaInicialMenuItem> MenuList { get; set; }
        public PaginaInicialViewModel InicialViewModel { get; set; }


        public ViewPaginaInicial(UsuarioModel usuario)
        {
            InitializeComponent();

            MenuList = new List<ViewPaginaInicialMenuItem>();
            InicialViewModel = new PaginaInicialViewModel(usuario);
            // Creating our pages for menu navigation
            // Here you can define title for item, 
            // icon on the left side, and page that you want to open after selection
            var page1 = new ViewPaginaInicialMenuItem() { Title = "Inicial", Icon = "home.png", TargetType = typeof(ViewPaginaInicialDetail) };
            var page2 = new ViewPaginaInicialMenuItem() { Title = "Meus treinos", Icon = "fa-archive", TargetType = typeof(ViewPaginaInicialDetail) };
            var page3 = new ViewPaginaInicialMenuItem() { Title = "Item 2", Icon = "fa-archive", TargetType = typeof(ViewPaginaInicialDetail) };
            var page4 = new ViewPaginaInicialMenuItem() { Title = "Item 4", Icon = "home.png", TargetType = typeof(ViewPaginaInicialDetail) };

            // Adding menu items to MenuList
            MenuList.Add(page1);
            MenuList.Add(page2);
            MenuList.Add(page3);


            // Setting our list to be ItemSource for ListView in MainPage.xaml
            navigationDrawerList.ItemsSource = MenuList;

            // Initial navigation, this can be used for our home page
            Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(ViewPaginaInicialDetail)))
            {
                BarBackgroundColor = Color.FromHex("#1f4e66"),

            };


            this.BindingContext = this.InicialViewModel;


        }

        // Event for Menu Item selection, here we are going to handle navigation based
        // on user selection in menu ListView
        public void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            var item = (ViewPaginaInicialMenuItem)e.SelectedItem;
            Type page = item.TargetType;

            Detail = new NavigationPage((Page)Activator.CreateInstance(page))
            {
                BarBackgroundColor = Color.FromHex("#1f4e66")
            };
            IsPresented = false;
        }

    }


}