﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppAcademiaDeBolso.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewPaginaInicialDetail : ContentPage
    {
        public ViewPaginaInicialDetail()
        {
            InitializeComponent();
        }
    }
}