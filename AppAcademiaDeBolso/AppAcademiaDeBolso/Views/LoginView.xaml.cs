﻿using AppAcademiaDeBolso.Models;
using AppAcademiaDeBolso.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppAcademiaDeBolso.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginView : ContentPage
	{
        public LoginViewModel LoginViewModel { get; set; }
        public LoginView ()
		{


            InitializeComponent();
            this.LoginViewModel = new LoginViewModel();
            this.BindingContext = this.LoginViewModel;

            
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Subscribers();
        }

        private void Subscribers()
        {
            MessagingCenter.Subscribe<LoginException>(this, "FalhaLogin",
                  async (exc) =>
                  {
                      await DisplayAlert("Login", exc.Message, "Ok");
                  });
          
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Unsubscribers();
        }

        private void Unsubscribers()
        {
            MessagingCenter.Unsubscribe<LoginException>(this, "FalhaLogin");
            MessagingCenter.Unsubscribe<UsuarioModel>(this, "SucessoLogin");
        }
    }
}