﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppAcademiaDeBolso.Views
{

    public class ViewPaginaInicialMenuItem
    {
        public ViewPaginaInicialMenuItem()
        {
            TargetType = typeof(ViewPaginaInicialDetail);
        }
        public string Title { get; set; }
        public string Icon { get; set; }
        public Type TargetType { get; set; }


    }
}