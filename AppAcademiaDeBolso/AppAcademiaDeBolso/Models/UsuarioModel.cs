
/* Script alterado por Marcus Brasizza   */
using	System;
using	System.Collections.Generic;
using	System.Text;


namespace	AppAcademiaDeBolso.Models
{
	public	class	UsuarioModel
	{
		private	int idUsuario;
		public	int IdUsuario
		    {
		        get {	return idUsuario;	}
		        set {	idUsuario = value;	}
		    }
		    
		private	string nomeUsuario;
		public	string NomeUsuario
		    {
		        get {	return nomeUsuario;	}
		        set {	nomeUsuario = value;	}
		    }
		    
		private	string emailUsuario;
		public	string EmailUsuario
		    {
		        get {	return emailUsuario;	}
		        set {	emailUsuario = value;	}
		    }
		    
		private	string senhaUsuario;
		public	string SenhaUsuario
		    {
		        get {	return senhaUsuario;	}
		        set {	senhaUsuario = value;	}
		    }
		    
		private	string cepUsuario;
		public	string CepUsuario
		    {
		        get {	return cepUsuario;	}
		        set {	cepUsuario = value;	}
		    }
		    
		private	string ruaUsuario;
		public	string RuaUsuario
		    {
		        get {	return ruaUsuario;	}
		        set {	ruaUsuario = value;	}
		    }
		    
		private	string numeroRuaUsuario;
		public	string NumeroRuaUsuario
		    {
		        get {	return numeroRuaUsuario;	}
		        set {	numeroRuaUsuario = value;	}
		    }
		    
		private	string complementoRuaUsuario;
		public	string ComplementoRuaUsuario
		    {
		        get {	return complementoRuaUsuario;	}
		        set {	complementoRuaUsuario = value;	}
		    }
		    
		private	string cidadeUsuario;
		public	string CidadeUsuario
		    {
		        get {	return cidadeUsuario;	}
		        set {	cidadeUsuario = value;	}
		    }
		    
		private	string estadoUsuario;
		public	string EstadoUsuario
		    {
		        get {	return estadoUsuario;	}
		        set {	estadoUsuario = value;	}
		    }
		    
		private	string paisUsuario;
		public	string PaisUsuario
		    {
		        get {	return paisUsuario;	}
		        set {	paisUsuario = value;	}
		    }
		    
		private	double latitude;
		public	double Latitude
		    {
		        get {	return latitude;	}
		        set {	latitude = value;	}
		    }
		    
		private	double longitude;
		public	double Longitude
		    {
		        get {	return longitude;	}
		        set {	longitude = value;	}
		    }
		    
		private	string facebookId;
		public	string FacebookId
		    {
		        get {	return facebookId;	}
		        set {	facebookId = value;	}
		    }
		    
		private	string fotoUsuario;
		public	string FotoUsuario
		    {
		        get {	return fotoUsuario;	}
		        set {	fotoUsuario = value;	}
		    }
		    
		private	string nivelUsuario;
		public	string NivelUsuario
		    {
		        get {	return nivelUsuario;	}
		        set {	nivelUsuario = value;	}
		    }
		    
		
	}
	
}
