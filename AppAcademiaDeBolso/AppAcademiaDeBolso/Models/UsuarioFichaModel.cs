
/* Script alterado por Marcus Brasizza   */
using	System;
using	System.Collections.Generic;
using	System.Text;


namespace	AppAcademiaDeBolso.Models
{
	public	class	UsuarioFichaModel
	{
		private	int idUsuarioFicha;
		public	int IdUsuarioFicha
		    {
		        get {	return idUsuarioFicha;	}
		        set {	idUsuarioFicha = value;	}
		    }
		    
		private	UsuarioModel usuario;
		public	UsuarioModel Usuario
		    {
		        get {	return usuario;	}
		        set {	usuario = value;	}
		    }
		    
		private	UsuarioModel professor;
		public	UsuarioModel Professor
		    {
		        get {	return professor;	}
		        set {	professor = value;	}
		    }
		    
		private	List<TreinoDiarioModel> listaTreino;
		public	List<TreinoDiarioModel> ListaTreino
		    {
		        get {	return listaTreino;	}
		        set {	listaTreino = value;	}
		    }
		    
		
	}
	
}
