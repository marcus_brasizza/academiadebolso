
/* Script alterado por Marcus Brasizza   */
using	System;
using	System.Collections.Generic;
using	System.Text;


namespace	AppAcademiaDeBolso.Models
{
	public	class	TreinoDiarioModel
	{
		private	int idTreinoDiario;
		public	int IdTreinoDiario
		    {
		        get {	return idTreinoDiario;	}
		        set {	idTreinoDiario = value;	}
		    }
		    
		private	UsuarioFichaModel usuarioFicha;
		public	UsuarioFichaModel UsuarioFicha
		    {
		        get {	return usuarioFicha;	}
		        set {	usuarioFicha = value;	}
		    }
		    
		private	DateTime dataTreino;
		public	DateTime DataTreino
		    {
		        get {	return dataTreino;	}
		        set {	dataTreino = value;	}
		    }
		    
		private	int quantidade;
		public	int Quantidade
		    {
		        get {	return quantidade;	}
		        set {	quantidade = value;	}
		    }
		    
		private	int descanso;
		public	int Descanso
		    {
		        get {	return descanso;	}
		        set {	descanso = value;	}
		    }
		    
		private	int tempo;
		public	int Tempo
		    {
		        get {	return tempo;	}
		        set {	tempo = value;	}
		    }
		    
		private	ExercicioModel exercicio;
		public	ExercicioModel Exercicio
		    {
		        get {	return exercicio;	}
		        set {	exercicio = value;	}
		    }
		    
		
	}
	
}
