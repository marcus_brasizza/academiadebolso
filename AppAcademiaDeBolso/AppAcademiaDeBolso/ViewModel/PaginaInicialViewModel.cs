﻿using AppAcademiaDeBolso.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppAcademiaDeBolso.ViewModel
{
    public class PaginaInicialViewModel
    {

        public UsuarioModel usuario { get; set; }
        public PaginaInicialViewModel(UsuarioModel usuario)
        {
            this.usuario = usuario;   
        }
    }
}
