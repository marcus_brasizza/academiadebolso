﻿using AppAcademiaDeBolso.Models;
using AppAcademiaDeBolso.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppAcademiaDeBolso.ViewModel
{
    public class LoginViewModel : BaseViewModel
    {
        private bool aguarde;

        public bool Aguarde
        {
            get { return aguarde; }
            set { aguarde = value;
                OnPropertyChanged(nameof(Aguarde));
            }
        }

        public ConfiguracoesModel Configuracoes { get; set; }
        public UsuarioModel Usuario { get; set; }
        private string email;

        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                OnPropertyChanged(nameof(Email));
            }
        }

        private string senha;

        public string Senha
        {
            get { return senha; }
            set
            {
                senha = value;
                OnPropertyChanged(nameof(Senha));
            }
        }
        public ICommand ValidarLoginCommand { get; set; }


        public LoginViewModel()
        {
            this.Aguarde = false;
            ValidarLoginCommand = new Command(async () =>
            {
                await OnSubmitLogin();
            });

            this.Configuracoes = new ConfiguracoesModel();
        }

        private async Task OnSubmitLogin()
        {
            try
            {
                this.Aguarde = true;
                var resultado = await FazerLogin(new LoginModel()
                {
                    Email = this.Email,
                    Senha = this.Senha
                });
                this.Aguarde = false;
                if (resultado.IsSuccessStatusCode)
                {

                    this.Usuario = JsonConvert.DeserializeObject<UsuarioModel>(resultado.Content.ReadAsStringAsync().Result);
                    MessagingCenter.Send<UsuarioModel>(this.Usuario, "SucessoLogin");
                }
                else
                {
                    MessagingCenter.Send<LoginException>(new LoginException("Login ou senha inválidos.", null), "FalhaLogin");
                }
            }
            catch (Exception exc)
            {
                
                MessagingCenter.Send<LoginException>(new LoginException("Erro de comunicação com o servidor.", exc), "FalhaLogin");
            }
        }

        public async Task<HttpResponseMessage> FazerLogin(LoginModel login)
        {
            using (var cliente = new HttpClient())
            {
                cliente.BaseAddress = new Uri(Configuracoes.UrlApi);
                var camposFormulario = new FormUrlEncodedContent(new[]
                {
                        new KeyValuePair<string, string>("email", login.Email),
                        new KeyValuePair<string, string>("senha", login.Senha)
                    });
                var resultado = await cliente.PostAsync("login.php", camposFormulario);
                return resultado;
            }
        }
    }

    public class LoginException : Exception
    {
        public LoginException() : base() { }

        public LoginException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
